export const xhr = (url, opts = {}) => {
  const storage = window.sessionStorage;
  const cachedResp = storage.getItem(url);
  const cacheTime = 30 * 1000;

  if (cachedResp) {
    return new Promise((resolve, reject) => resolve(JSON.parse(cachedResp)));
  }

  return fetch(url, opts)
    .then(res => {
      res.clone().text().then(data => storage.setItem(url, data));
      window.setTimeout(() => {
        storage.removeItem(url);
      }, cacheTime);
      return res.json();
    });
}