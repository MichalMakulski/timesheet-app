import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import ErrPage from './ErrPage';
import './index.css';
import { api } from './api';
import { xhr } from './utils';

const render = (err, data) => {
  const content = err ? <ErrPage err={err} /> : <App users={data} />;
  ReactDOM.render(content, document.getElementById('root'));
}

xhr(api.users)
  .then(users => render(null, users))
  .catch(err => render(err));