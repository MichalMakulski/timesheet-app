import React, { Component } from 'react';
import { Header } from './Header';
import { Body } from './Body';
import { api } from '../../api';
import { xhr } from '../../utils';

export default class Calendar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentMonth: new Date().getMonth() + 1,
      currentYear: new Date().getFullYear(),
      currentMonthData: []
    }
  }

  componentWillReceiveProps(nextProps) {
    if(!nextProps.currentUser) {
      return this.forceUpdate();
    }

    if (nextProps.currentUser !== this.props.currentUser) {
      this.updateCalendarData(nextProps.currentUser.id);
    }
  }

  updateCalendarData(userId) {
    const {currentMonth, currentYear} = this.state;
    xhr(`${api.weeks}/${currentMonth}/${currentYear}/${userId}`)
      .then(calendarData => this.setState({currentMonthData: calendarData.data.weeks}))
  }

  render() {
    const {currentMonth, currentYear, currentMonthData} = this.state;
    const {currentUser} = this.props;

    if (!currentUser) {
      return null
    }

    return (
      <div>
        <Header
          user={currentUser.username}
          month={currentMonth}
          year={currentYear}
        />
        <Body monthData={currentMonthData} />
      </div>
    )
  }
}