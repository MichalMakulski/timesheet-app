import React from 'react';
import './Body.css';

const getCalendarContent = (monthData) => {
  return monthData.map(week => {
    const days = week.days_in_week.map(day => <Day key={day.id} day={day.day_number} />);
    return <Week key={week.week_id}>{days}</Week>
  })
}

export const Week = ({children, isHeader}) => {
  const className = isHeader ? 'Week Week__header' : 'Week';
  return (
    <div className={className}>{children}</div>
  )
}
export const Day = ({day}) => {
  return (
    <div className="Day">{day}</div>
  )
}
export const Body = ({monthData}) => {
  const calendarContent = getCalendarContent(monthData);
  return (
    <div className="Body">
      {calendarContent}
    </div>
  )
}