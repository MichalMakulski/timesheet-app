import React from 'react';
import {Day, Week} from './Body';

export const Header = ({user, month, year}) => {
  const months = [null, 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  const days = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
  const daysHeader = days.map((day, idx) => <Day key={idx} day={day} />);

  return (
    <div>
      <h2>{`${months[month]} ${year}`}</h2>
      <h4>{`${user}'s calendar:`}</h4>
      <Week isHeader={true}>{daysHeader}</Week>
    </div>
  )
}