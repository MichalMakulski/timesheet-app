import React from 'react';
import './UserSelect.css';

const UserOption = ({id, username}) => {
  return (
    <option value={id}>{username}</option>
  )
}

export const UserSelect = ({users, currentUser, onUserSelect}) => {
  const usersOptions = users.map(user => <UserOption key={user.id} {...user} />);
  return (
    <div className="UserSelect">
      <span>Users:</span>
      <select
        value={currentUser.username}
        onChange={onUserSelect} >
        <option value="">Pick a user...</option>
        {usersOptions}
      </select>
    </div>
  )
}