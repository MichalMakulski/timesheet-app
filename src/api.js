export const api = {
  users: 'https://timesheet-staging-aurity.herokuapp.com/api/users',
  weeks: 'https://timesheet-staging-aurity.herokuapp.com/api/training/weeks'
}