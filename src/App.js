import React, { Component } from 'react';
import { Calendar, UserSelect } from './components';
import './App.css';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentUser: null
    }
  }

  onUserSelect = e => {
    const userId = e.target.value;
    const currentUser = this.props.users.filter(user => user.id.toString() === userId)[0] || null;
    this.setState({currentUser});
  }

  render() {
    const {currentUser} = this.state;
    const {users} = this.props;
    return (
      <div className="App">
        <div className="App-header">
          <h1>Timesheets App</h1>
          <UserSelect
            users={users}
            currentUser={currentUser ? currentUser.username : ''}
            onUserSelect={this.onUserSelect}
          />
        </div>
        <Calendar currentUser={currentUser} />
      </div>
    );
  }
}