import React from 'react';

const ErrPage = ({err}) => {
  const styles = {color: 'red', backgroundColor:'pink', padding: 10};
  return (
    <p style={styles}>
      {`Something went wrong: ${err.message}`}
    </p>
  )
}

export default ErrPage;